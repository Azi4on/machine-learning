# **Набор данных о машинах**

## Источник

<https://www.kaggle.com/nehalbirla/vehicle-dataset-from-cardekho?select=Car+details+v3.csv>

## Описание

Этот набор данных содержит информацию о подержанных автомобилях.

## Задача

Предсказать цену машины. Целевая переменная - Selling price.

## Описание признаков

1. name: название машины; string.
2. year: год производства; int.
3. selling_price: цена продажи; int.
4. fuel: тип топлива; string.
5. seller_type: как продаётся; string.
6. transmission: string.
7. owner: владелец; string.
8. mileage: пробег; string
9. engine: двигатель; string.
10. max_power: максимальная мощность; string.
11. torque: крутящий момент; string.
12. seats: число мест; float.
